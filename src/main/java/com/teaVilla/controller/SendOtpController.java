package com.teaVilla.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.teaVilla.models.SystemOtp;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;


@RestController
public class SendOtpController {
	
	// should not we duplicate 
	 private Map<String,SystemOtp> otp= new HashMap();
	 
	 private static final String Account_id="AC0c399a167b3f5e9ed99448c91efe8bde" ;
	 private static final String auth_id ="676d9196d448518a914154ced951f8ba";
	 
	 static {
		Twilio.init(Account_id, auth_id);
	 }
	 
	 @GetMapping
	 public String show() {
		 return "Rs.9999/-";
	 }
	 
   @RequestMapping(value="/mobileNumber/{mobileNumber}/otp",method = RequestMethod.POST)
    public ResponseEntity<Object> sendOtp(@PathVariable("mobileNumber")String mobileNumber){
	   SystemOtp systemOtp= new SystemOtp();
	   systemOtp.setMobileNumber(mobileNumber);
	   systemOtp.setOtp(String.valueOf(((int)(Math.random()*(10000 - 1000)))*1000));
	   systemOtp.setExpiryTime(System.currentTimeMillis()+20000);
	   otp.put(mobileNumber,systemOtp);
	   Message.creator(new PhoneNumber("+917829017731"), new PhoneNumber("+12565734379"),"your otp is"+systemOtp.getOtp()).create();
	   		return new ResponseEntity<>("otp send successfully",HttpStatus.OK);
		}
   @RequestMapping(value="/mobileNumber/{mobileNumber}/otp",method = RequestMethod.PUT)
   public ResponseEntity<Object> verifyOtp(@PathVariable("mobileNumber") String mobileNumber,@RequestBody SystemOtp responseBodySystemOtp){
	 
	   if(responseBodySystemOtp.getOtp() == null || responseBodySystemOtp.getOtp().trim().length()<=0) {
		   return new ResponseEntity<> ("please provied the otp",HttpStatus.BAD_REQUEST);
		   
	   }
	   
	   // here just the mobile number is their or not
	   if(otp.containsKey(mobileNumber)) {
		   // here get the mobile number
		   SystemOtp systemOtp=otp.get(mobileNumber);
		   if(systemOtp!=null) {
			   if(systemOtp.getExpiryTime()>=System.currentTimeMillis()) {
				 if(responseBodySystemOtp.getOtp().equals(systemOtp.getOtp())) {  
					 return new ResponseEntity<> ("valid Otp",HttpStatus.OK);
				 }
				   return new ResponseEntity<>("",HttpStatus.BAD_REQUEST);
			   }
			   return new ResponseEntity<>("some thing worong",HttpStatus.BAD_REQUEST);
		   }
		   return new ResponseEntity<>("Some >=thing wrong",HttpStatus.BAD_REQUEST);
	   }
	return  new ResponseEntity<>("Mobile Number Not Found",HttpStatus.NOT_FOUND); 
	   
   }
}














