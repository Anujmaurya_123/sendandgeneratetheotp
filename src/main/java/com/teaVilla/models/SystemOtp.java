package com.teaVilla.models;

public class SystemOtp {
	private String mobileNumber;
	private String otp;
	private long expiryTime;
	
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public long getExpiryTime() {
		return expiryTime;
	}
	public void setExpiryTime(long expiryTime) {
		this.expiryTime = expiryTime;
	}
	@Override
	public String toString() {
		return "SystemOpt [mobileNumber=" + mobileNumber + ", otp=" + otp + ", expiryTime=" + expiryTime + "]";
	}
	
	

}
