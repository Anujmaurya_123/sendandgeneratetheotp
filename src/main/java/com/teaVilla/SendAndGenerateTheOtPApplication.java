package  com.teaVilla;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SendAndGenerateTheOtPApplication {

	public static void main(String[] args) {
		SpringApplication.run(SendAndGenerateTheOtPApplication.class, args);
	}

}
